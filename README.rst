========================
SparkInCrowd 
========================

SparkInCrowd is assement tools for building truth dataset.


Requirement
=================

- PHP
- MySQL


Setup project
=================

SparkInCrowd is open source project publically available on Bitbucket. To clone the repository, use the following command.

.. code-block:: bash

	git clone https://bitbucket.org/sparkindata-irit/sparkincrowd.git


Create a MySQL Database and load `agrinote.sql <./agrinote.sql>`_.



Data assement
=================

Assessors are asked to read a sentence and select answers for two questions:

- **Cycle maladie?** inconnu, d�but, en d�veloppement, fin
- **Intensiti� de la maladie?** inconnu, inexistant, faible, fort, tr�s fort



Get the data
=================

Assessment results are available in *agrinote_manual_assesment* MySQL table. The following table describe each field.


+---------------+------------------------------------------------------------------------------+
| Field         | Description                                                                  |
+===============+==============================================================================+
| assesment_id  | Incremental identifier of assessment                                         |
+---------------+------------------------------------------------------------------------------+
| datetime      | The date and the time of assessment                                          |
+---------------+------------------------------------------------------------------------------+
| record_id     | The identifier of the sentence, see  agrinote_record table for full details  |
+---------------+------------------------------------------------------------------------------+
| q1            | The response of the assessor on the first question                           |
+---------------+------------------------------------------------------------------------------+
| q2            | The response of the assessor on the second question                          |
+---------------+------------------------------------------------------------------------------+
| user          | The name of the assessor if mentionned                                       |
+---------------+------------------------------------------------------------------------------+
| ip            | The ip of the assessor                                                       |
+---------------+------------------------------------------------------------------------------+
| user_agent    | The user agent of the assessor                                               |
+---------------+------------------------------------------------------------------------------+

Version
===============

SparkInCrowd 0.0.1


Contributors
===============

The following people have contributed to this code:

- Lamjed Ben Jabeur `Lamjed.Ben-Jabeur@irit.fr <mailto:Lamjed.Ben-Jabeur@irit.fr>`_.

License
===============
This software is governed by the `CeCILL-B license <LICENSE.txt>`_ under French law and abiding by the rules of distribution of free software.  You can  use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL
`http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html <http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>`_.
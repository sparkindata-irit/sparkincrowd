<?php
session_start();  

//error_reporting(E_ALL);
//ini_set("display_errors", 1);


define('HOME_URL','http://ns3038079.ip-5-135-160.eu/agrinote/');


if(isset($_REQUEST['user']) && $_REQUEST['user']!=''){
	$_SESSION['user']=$_REQUEST['user'];
	setcookie( "user", $_REQUEST['user'], strtotime( '+90 days' ) ); 
}

$user= (isset($_REQUEST['user']) && $_REQUEST['user']!='') ?  $_REQUEST['user'] : ( (isset($_SESSION['user']) && $_SESSION['user']!='') ?  $_SESSION['user'] : (  (isset($_COOKIE['user']) && $_COOKIE['user']!='') ?  $_COOKIE['user'] : "" )  );

$debug=false;
/*
CREATE OR REPLACE VIEW agrinote_entity_popularity AS
SELECT entity, COUNT(*) as popularity
FROM agrinote_mention_entity_tweet
GROUP BY entity;*/
$link=mysqli_connect('localhost', 'agrinote', 'agrinote', 'agrinote');
mysqli_set_charset($link, 'utf8');
$debug=false;

 
if(isset($_REQUEST['id']) && $_REQUEST['id']!=''
		&& isset($_REQUEST['q1']) && $_REQUEST['q1']!=''
		&& isset($_REQUEST['q2']) && $_REQUEST['q2']!='') {
	
	$sql = 'INSERT INTO agrinote_manual_assesment (record_id,q1,q2,user,ip,user_agent) VALUES ('.mysqli_real_escape_string($link,$_REQUEST['id']).',"'.mysqli_real_escape_string($link,$_REQUEST['q1']).'","'.mysqli_real_escape_string($link,$_REQUEST['q2']).'","'.mysqli_real_escape_string($link,$_REQUEST['user']).'","'.mysqli_real_escape_string($link,$_SERVER['REMOTE_ADDR']).'","'.mysqli_real_escape_string($link,$_SERVER['HTTP_USER_AGENT']).'")';
	$result = mysqli_query($link,$sql);
}

if(isset($_REQUEST['ignore']) && $_REQUEST['ignore']!='') {
	$sql = 'INSERT INTO agrinote_ignore (record_id,ip,user_agent) VALUES ('.mysqli_real_escape_string($link,$_REQUEST['ignore']).',"'.mysqli_real_escape_string($link,$_SERVER['REMOTE_ADDR']).'","'.mysqli_real_escape_string($link,$_SERVER['HTTP_USER_AGENT']).'")';
	$result = mysqli_query($link,$sql);
}




$count_done=0;
$sql    = 'SELECT COUNT(DISTINCT(record_id)) as nbr  FROM agrinote_manual_assesment';
$result = mysqli_query($link,$sql);
if ($row = mysqli_fetch_array($result)){
	$count_done=$row['nbr'];
}

$count_all=0;
$sql    = 'SELECT COUNT(*) as nbr  FROM agrinote_record';
$result = mysqli_query($link,$sql);
if ($row = mysqli_fetch_array($result)){
	$count_all=$row['nbr'];
}




$sql    = 'SELECT r.* FROM agrinote_record r  LEFT JOIN agrinote_manual_assesment a ON r.id=a.record_id  ORDER BY RAND() DESC LIMIT 0,1;';
$result = mysqli_query($link,$sql);

$data=[];
if ($row = mysqli_fetch_array($result)){
	$data=$row;
	$id=$row['id'];
	$disease=$row['disease'];
	$location=$row['location'];
	$date=$row['date'];
	$text=$row['text'];
}else{
	header('Location: '.HOME_URL);
}


?><!DOCTYPE html>
<html lang="ar">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Agrinote</title>

    <!-- Bootstrap core CSS -->
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css" rel="stylesheet">
	
	<link href='http://fonts.googleapis.com/earlyaccess/droidarabickufi.css' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/earlyaccess/amiri.css' rel='stylesheet' type='text/css'>
	
	<link href="css/styles.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="http://getbootstrap.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://getbootstrap.com/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

   <?php if ($debug) : ?>
	<pre><?php echo print_r($tweet,true); ?></pre>
	<?php endif; ?>
	
	
	
<div class="container" style="text-align:center;">
	<div style="max-width:600px;width:auto; margin:0px auto;">
		<form action="index.php<?php echo (isset($_REQUEST['p']))? '?p='.$_REQUEST['p'] : ''?>" method="post">

			<div class="row">			
				<div class="fform" style="padding:0px 20px;">
					<div class="form-group" style="max-width:300px;margin:0px auto;">
						<div class="input-group";">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Participant
							</div>
							<input type="text" name="user"  class="form-control" value="<?php echo isset($user)? $user : ""?>" placeholder="Saisir votre nom">
						</div>
					</div>
				</div>
			</div>
			<br/><br/>
			<div class="row">	
				<div class="twty"  style="max-width:500px;padding:0px 10px; margin:0px auto;">
				<h2 style="color:#337ab7;"> <span class="glyphicon glyphicon-leaf" aria-hidden="true"></span> Maladie : <?php echo $data['disease'] ?></h2>
				<br/><br/>
				<p>Extrait de la sitation de la maladie:</p>
				<blockquote class="twitter-tweet">
					<p style="font-size:x-large;"><?php echo $data['text'] ?></p>
					<footer>
						Bulletin de santé du végétal <span class="glyphicon glyphicon-map-marker" aria-hidden="true"><cite><?php echo $data['location'] ?></cite></span> <span class="glyphicon glyphicon-time" aria-hidden="true"><cite><?php echo $data['date'] ?></cite></span>
						</footer>
				</blockquote>
				</div>
			</div>

			<div class="row">
				<div class="fform" style="padding:0px 20px;">
								
								<input type="hidden" name="id" value="<?php echo $data['id'] ?>" />
								<div  class="ffrom-question  ffrom1" style="margin-top:20px;"> 
									<p  style="font-size:large;">Cycle  maladie?</p>
									<div class="btn-group" data-toggle="buttons" >
										<label class="btn btn-mg btn-default">
										<input type="radio" name="q1" id="related" value="-1" required>inconnu</label>
										<label class="btn btn-mg btn-default">
										<input type="radio" name="q1" id="related" value="1" required>début</label>
										<label class="btn btn-mg btn-default">
										<input type="radio" name="q1" id="related" value="2" required>en développement</label>
										<label class="btn btn-mg btn-default">
										<input type="radio" name="q1" id="related" value="3" required>fin</label>
									</div>
								</div>
								
								
								<div  class="ffrom-question ffrom2" style="margin-top:25px;"> 
									<p  style="font-size:large;">Intensitié de la maladie?</p>
									<div class="btn-group" data-toggle="buttons" >
										<label class="btn btn-mg btn-default">
										<input type="radio" name="q2" id="topic" value="-1" required>inconnu</label>
										<label class="btn btn-mg btn-default">
										<input type="radio" name="q2" id="topic" value="0" required>inexistant</label>
										<label class="btn btn-mg btn-default">
										<input type="radio" name="q2" id="topic" value="1" required>faible</label>
										<label class="btn btn-mg btn-default">
										<input type="radio" name="q2" id="topic" value="2" required>fort</label>
										<label class="btn btn-mg btn-default">
										<input type="radio" name="q2" id="topic" value="3" required>très fort</label>
									</div>
								</div>
								
							<br/>
							<br/>
				  <a href="<?php echo HOME_URL ?>?ignore=<?php echo $data['id'] ?>" class="btn btn-lg  btn-default">Ignorer</a>
				  <button type="submit" class="btn btn-lg  btn-primary">Valider</button>
						
				</div>
			</div>
			<br/><br/>
			<div class="row">
				<div class="fform" style="padding:0px 20px;">
				<hr/>
					<small><?php echo $count_done ?> / <?php echo $count_all ?> phrases ont été analysées.</small>
				  </div>
			</div>
		</form>
	</div>
</div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <script src="js/tnelec.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
	<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
  <script>
 jQuery(function($){
	$(document ).ready(function() {
		$("[name ='related']").change(function(){
			var val=$("input:radio[name ='related']:checked").val();
			if(val=='yes'){
				$(".ffrom2").removeClass("hide");
				$(".ffrom3").removeClass("hide");
				$(".ffrom4").removeClass("hide");
			}else{
				$(".ffrom2").addClass("hide");
				$(".ffrom3").addClass("hide");
				$(".ffrom4").addClass("hide");
			}
		});
		$("[name ='sentiment']").change(function(){
			var val=$("input:radio[name ='sentiment']:checked").val();
			if(val!='negative' && val!='mixed'){
				$(".ffrom4").addClass("hide");
			}else{
				$(".ffrom4").removeClass("hide");
			}
		});
	});
});
  </script>
  </body>
</html>
